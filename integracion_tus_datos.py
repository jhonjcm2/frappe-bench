import requests

class IntegracionTusDatos:
    """Clase cliente que permite la integracion del api tusDatos.

    Mediante está clase se permite la integración de tusDatos con base  
    a la documentacion del api http://docs.tusdatos.co/docs
    """


    def __init__(self, parameter_list):
        """Constructor Clase IntegracionTusDatos.

        Se debe pasar un arreglo asociativo, el cual debe contener los siguientes campos
        obligatorios {url,launch,verify,launch_car,retry,results,report,report_pdf,report_nit,
        report_nit_pdf,report_json,user,passw,headers}
        """
        self.url = parameter_list["url"]
        self.launch = parameter_list["launch"]
        self.verify = parameter_list["verify"]
        self.launch_car = parameter_list["launch_car"]
        self.retry = parameter_list["retry"]
        self.results = parameter_list["results"]
        self.report = parameter_list["report"]
        self.report_pdf = parameter_list["report_pdf"]
        self.report_nit = parameter_list["report_nit"]
        self.report_nit_pdf = parameter_list["report_nit_pdf"]
        self.report_json = parameter_list["report_json"]
        self.user = parameter_list["user"]
        self.passw = parameter_list["passw"]
        self.headers = parameter_list["headers"]
        

    def apiLaunch(self, parameter_list):
        """
        EndPoind que realiza peticiones para iniciar consultas.

        consulta sobre el documento deseado indicando su tipo (CC, CE, NIT, PP, NOMBRE).
        La consulta se realiza en base al perfil asignado al usuario.
        Si la consulta ya fue realizada previamente tendrá una respuesta diferente, aplica para el caso de consultas de cédulas onsultadas en los últimos 15 días. 
        """
        auth = (self.user, self.passw)
        headers = {'Content-Type': self.headers}

        data = {}
        try:
            data["doc"] = parameter_list["doc"]
        except KeyError:
            print("[doc] is unknown")    

        try:
            data["typedoc"] = parameter_list["typedoc"]
        except KeyError:
            print("[typedoc] is unknown")

        try:
            data["fechaE"] = parameter_list["fechaE"]
        except KeyError:
            print("[fechaE] is unknown")    

        try:
            data["nombre"] = parameter_list["nombre"]
        except KeyError:
            print("[nombre] is unknown")    
        
        #print(data)
        #data = { "doc": 123, "typedoc": parameter_list["typedoc"], "fechaE": parameter_list["fechaE"] }
        response = requests.post(url=self.url + self.launch, headers=headers, auth=auth, json=data)

        print(data)

        result = self.validateResponse(response)
        return result


    def validateResponse(self, response):
        try:
            response.raise_for_status()
            if response.status_code == 100:
                return {"error":True,"msg":"100 Continue", "data":response.text }
            if response.status_code == 200:
                return {"error":False, "msg":"Succes" ,"data":response.text }
            if response.status_code == 300:
                return {"error":True,"msg":"300 Multiple Choices", "data":response.text }
            if response.status_code == 422:
                return {"error":True,"msg":"422 Unprocessable Entity", "data":response.text }
            if response.status_code == 500:    
                return {"error":True,"msg":"500 Internal Server Error", "data":response.text }
            else:
                return {"error":True,"msg":"code is unknown", "data":response.text }        
        except Exception as err:
            return {"error":True,"msg":err, "data":response.text }

    


#parameter_list = []
parameter_list = {}
parameter_list["url"] = "http://docs.tusdatos.co"
parameter_list["launch"] = "/api/launch"
parameter_list["verify"] = "/api/launch/verify"
parameter_list["launch_car"] = "/api/launch/car"
parameter_list["retry"] = "/api/retry/id"
parameter_list["results"] = "/api/results/jobkey"
parameter_list["report"] = "/api/report/id"
parameter_list["report_pdf"] = "/api/report_pdf/id"
parameter_list["report_nit"] = "/api/report_nit/id"
parameter_list["report_nit_pdf"] = "/api/report_nit_pdf/id"
parameter_list["report_json"] = "/api/report_json/id"
parameter_list["user"] = "pruebas"
parameter_list["passw"] = "password"
parameter_list["headers"] = "application/json"

integracion = IntegracionTusDatos(parameter_list)

parameter_list = {}
parameter_list["doc"] = "666"
parameter_list["typedoc"] = "CC"
parameter_list["fechaE"] = "01/12/2011"
parameter_list["nombre"] = "javier"

resp = integracion.apiLaunch(parameter_list)
print(resp)

#print(IntegracionTusDatos.__init__.__doc__)