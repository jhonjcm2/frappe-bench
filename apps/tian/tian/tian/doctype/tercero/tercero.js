// Copyright (c) 2020, Fianzacrédito and contributors
// For license information, please see license.txt

frappe.ui.form.on('Tercero', {
	tipo_persona: function (frm) {
		let todos = [
			'fotocopia_cedula',
			'fotocopia_cedula_representante_legal',
			'extractos_bancarios1',
			'extractos_bancarios2',
			'extractos_bancarios3',
			'certificacion_laboral',
			'inscripcion_camara_comercio',
			'fotocopia_rut',
			'fotocopia_declaracion_renta',
			'fotocopia_estados_financieros',
			'certificado_pension_o_ultimo_pago',
			'certificado_existencia',
			'certificado_representacion_legal',
		];

		let documentos = [];

		if (frm.doc.tipo_persona == 'Empleado') {
			documentos = [
				'fotocopia_cedula',
				'certificacion_laboral'
			];
		} else if (frm.doc.tipo_persona == 'Independiente') {
			documentos = [
				'fotocopia_cedula',
				'extractos_bancarios1',
				'extractos_bancarios2',
				'extractos_bancarios3',
				'inscripcion_camara_comercio',
				'fotocopia_rut',
				'fotocopia_declaracion_renta',
			];
		} else if (frm.doc.tipo_persona == 'Pensionado') {
			documentos = [
				'fotocopia_cedula',
				'extractos_bancarios1',
				'extractos_bancarios2',
				'extractos_bancarios3',
				'certificado_pension_o_ultimo_pago',
			];
		} else if (frm.doc.tipo_persona == 'Persona Jurídica') {
			documentos = [
				'fotocopia_cedula_representante_legal',
				'extractos_bancarios1',
				'extractos_bancarios2',
				'extractos_bancarios3',
				'fotocopia_rut',
				'fotocopia_declaracion_renta',
				'fotocopia_estados_financieros',
				'certificado_existencia',
			];
		}

		todos.forEach(doc => {
			frm.toggle_reqd(doc, false);
			frm.toggle_display(doc, false);
		});

		documentos.forEach(doc => {
			frm.toggle_reqd(doc, true);
			frm.toggle_display(doc, true);
		});
	}
});
