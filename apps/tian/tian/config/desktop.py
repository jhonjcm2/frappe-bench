# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "TIAN",
			"color": "grey",
			"icon": "fa fa-file-o",
			"type": "module",
			"label": _("TIAN")
		}
	]
