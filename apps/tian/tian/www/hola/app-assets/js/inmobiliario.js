Vue.use(VueFormWizard)

new Vue({
    el: "#app",
    data() {
        return {
            title: 'Solicitud de Fianza',
            subtitle: 'Ingrese la información para solicitar su fianza',
            formValues: {
                condiciones: [
                    { label: "Empleado", value: "Empleado" },
                    { label: "Independiente", value: "Independiente" },
                    { label: "Pensionado", value: "Pensionado" },
                    { label: "Persona Jurídica", value: "Persona Jurídica" },
                ],
                tipo_identificacion: [
                    { label: "Cédula Ciudadanía", value: "Cédula Ciudadanía" },
                    { label: "Cédula Extranjería", value: "Cédula Extranjería" },
                    { label: "Pasaporte", value: "Pasaporte" },
                    { label: "NIT", value: "NIT" },
                    { label: "RUT", value: "RUT" },
                ],
                estado_civil: [
                    { label: "Soltero", value: "Soltero" },
                    { label: "Casado", value: "Casado" },
                ],
                estrato: [
                    { label: "1", value: "1" },
                    { label: "2", value: "2" },
                    { label: "3", value: "3" },
                    { label: "4", value: "4" },
                    { label: "5", value: "5" },
                    { label: "6", value: "6" },
                    { label: "7", value: "7" },
                ],
                tipo_uso: [
                    { label: "Vivienda", value: "Vivienda" },
                    { label: "Comercial", value: "Comercial" },
                    { label: "Mixto", value: "Mixto" },
                ]
            },
            cliente: {

            },
            inmueble: {

            },
            inmuebles: {

            },
            vehiculos: {

            },
            titulos_valores: {

            },
            referencias_documentos: {

            },
            referencias_personales: {},
            referencias_comerciales: {},
            fiadores: {},
            rules: {
                condicion: [{
                    required: false,
                    message: 'Please input Activity name',
                    trigger: 'blur'
                }, {
                    min: 3,
                    max: 5,
                    message: 'Length should be 3 to 5',
                    trigger: 'blur'
                }],
                identificacion: [{
                    required: false,
                    message: 'Please select Activity zone',
                    trigger: 'change'
                }],
            }
        }
    },
    methods: {
        onComplete: function () {
            alert('Yay. Done!');
        },
        validateFirstStep() {
            return new Promise((resolve, reject) => {
                this.$refs.cliente.validate((valid) => {
                    resolve(valid);
                });
            })

        }
    }
});