# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('requirements.txt') as f:
	install_requires = f.read().strip().split('\n')

# get version from __version__ variable in tian/__init__.py
from tian import __version__ as version

setup(
	name='tian',
	version=version,
	description='Sistema de gestión de Fianzacrédito S.A',
	author='Fianzacrédito',
	author_email='digital@fianzacredito.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
